import networkx as nx
import osmnx as ox
import matplotlib.pyplot as plt
import random

# Getting a map of Montreal
G = ox.graph_from_place("LaSalle, Montreal, Canada", network_type="drive")
fig, ax = ox.plot_graph(G,  figsize=(10, 10))

# To eulerize the graph representing Montreal,
# we need to convert it from MultiDiGraph to MultiGraph
U = G.to_undirected()

# We can now eulerize
H = nx.eulerize(U)

# And check if it is eulerian
nx.is_eulerian(H)

cycle = nx.eulerian_circuit(H)

def edgeToStreetName(G, edge):
    src, trgt = edge
    for (u, v, w) in G.edges(data = True):
        # print(w)
        if u == src and v == trgt:
            if 'name' in w.keys():
                return w['name']

def printCycle(cycle, G):
    for e in cycle:
        res = edgeToStreetName(G, e)
        if res != None:
            print(res)
printCycle(cycle, H)
