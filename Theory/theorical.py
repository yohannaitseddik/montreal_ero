import copy
from random import randrange

import numpy as np
from scipy.optimize import linear_sum_assignment
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import dijkstra


def graphToAdjMatrix(graph, n):
    res = []
    for i in range(n):
        tmp = []
        for j in range(n):
            if j in graph[i]:
                tmp.append(1)
            else:
                tmp.append(0)
        res.append(tmp)
    return res


def myGraphToAdjMatrix(myGraph, n):
    res = []
    for i in range(n):
        tmp = []
        for j in range(n):
            if (i, j) in myGraph:
                tmp.append(myGraph[(i, j)])
            elif (j, i) in myGraph:
                tmp.append(myGraph[(j, i)])
            else:
                tmp.append(0)
        res.append(tmp)
    return res


def generateRandomMatrix(n, max_neige):
    res1 = {}
    res2 = {}
    for i in range(n * 4):
        if i < n:
            x = (i, randrange(n))
        else:
            x = (randrange(n), randrange(n))
        res1[x] = randrange(1, 50)
        res2[x] = randrange(max_neige)
    return res1, res2


def deleteDouble(obj, snow):
    obj2 = copy.copy(obj)
    visited = []
    for (a, b) in obj:
        if (b, a) in visited:
            obj2.pop((b, a))
        elif a == b:
            obj2.pop((a, a))
        elif snow[(a, b)] < 2.5 or snow[(a, b)] > 15:
            obj2.pop((a, b))
        else:
            visited.append((a, b))
    return obj2

def printSnow(dist, snow):
    print("SNOW :")
    for i in dist.keys():
        print(i, ": ", snow[i], end=', ')
    print()


def printAdj(obj, n):
    for i in range(n):
        if i == n - 1:
            print(obj[i])
        else:
            print(obj[i], ',')


def all_dijkstra(arr, size):
    res = []
    for i in range(size):
        d = dijkstra(csgraph=arr, directed=True, indices=i, min_only=True)
        d.itemset(i, 9999)
        res.append(d)
    return np.array(res)


def oddVertices(adjMat, n):
    res = []
    for i in range(n):
        degree = 0
        for j in range(n):
            if adjMat[i][j] != 0:
                degree += 1
        if degree % 2 != 0:
            res.append(i)
    return res


def reduceMatrix(oddList, distMatrix):
    res = []
    for i in oddList:
        tmp = []
        for j in oddList:
            tmp.append(distMatrix[i][j])
        res.append(tmp)
    return np.array(res)


def createLink(linearSum, oddList, graph, distMatrix):
    size = linearSum[0].size
    visited = []
    for i in range(size):
        a = oddList[linearSum[0][i]]
        b = oddList[linearSum[1][i]]
        if (b, a) not in visited:
            if graph.keys().__contains__((a, b)) and graph.__contains__((b, a)):
                raise Exception("Big Problem Create Link")
            if graph.keys().__contains__((a, b)):
                graph[(b, a)] = distMatrix[a][b]
            else:
                graph[(a, b)] = distMatrix[a][b]
            visited.append((a, b))
    return graph


def toPythonGraph(matriceFinale, n):
    res = {}
    for i in range(n):
        res[i] = []
    for key in matriceFinale.keys():
        res[key[0]].append(key[1])
        res[key[1]].append(key[0])
    return res


def dfs(u, graph, visited_edge, path=[]):
    path = path + [u]
    for v in graph[u]:
        if visited_edge[u][v] != 0:
            visited_edge[u][v] -= 1
            visited_edge[v][u] -= 1
            path = dfs(v, graph, visited_edge, path)
    return path


def check_circuit_or_path(graph, max_node):
    odd_degree_nodes = 0
    odd_node = -1
    for i in range(max_node):
        if i not in graph.keys():
            continue
        if len(graph[i]) % 2 == 1:
            odd_degree_nodes += 1
            odd_node = i
    if odd_degree_nodes == 0:
        return 1, odd_node
    if odd_degree_nodes == 2:
        return 2, odd_node
    return 3, odd_node


def check_euler(graph, max_node, newAdjMatrix):
    visited_edge = newAdjMatrix
    check, odd_node = check_circuit_or_path(graph, max_node)
    if check == 3 or check == 2:
        print("Error occur")
        return
    start_node = 0
    path = dfs(start_node, graph, visited_edge)
    return path


def numberKm(finalGraph, path, dist_matrix):
    res = 0
    if path is not None:
        for i in range(len(path) - 1):
            key = (path[i], path[i + 1])
            invKey = (path[i + 1], path[i])
            if finalGraph.__contains__(key):
                res += finalGraph[key]
            elif finalGraph.__contains__(invKey):
                res += finalGraph[invKey]
            else:
                res += dist_matrix[path[i]][path[i + 1]]
    return res


def newAdjMatrix(pythonGraph, max_node):
    res = [[0 for _ in range(max_node)] for _ in range(max_node)]
    for i in range(max_node):
        for j in pythonGraph[i]:
            res[i][j] += 1
    return res


n = 5
maxNeige = 25

randMat_dist, randMat_neige = generateRandomMatrix(n, maxNeige)
randMat_dist = deleteDouble(randMat_dist, randMat_neige)  # Delete the Street with snow < 2.5 and snow > 15
print('randMat DIST :\n', randMat_dist)
printSnow(randMat_dist, randMat_neige)

adjMatrix = myGraphToAdjMatrix(randMat_dist, n)  # Change the Matrix in a adjMatrix (Needed to use scipy)
arr2 = np.array(adjMatrix)
newarr2 = csr_matrix(arr2)  # Transform the adjMatrix in a csrMatrix to use the dijkstra function

# print("ADJ Matrix :\n")
# printAdj(arr2, n)
print("CSR MATRIX :\n", newarr2)

distMatrix = all_dijkstra(newarr2, n)  # Create a dist matrix
print("DIST MATRIX:\n", distMatrix)

oddVerticesList = oddVertices(adjMatrix, n)   # Check the list of point with odd number of link
reducedMatrix = reduceMatrix(oddVerticesList, distMatrix)  # Reduce the distMatrix to the points that we need
print("LIST OF POINT TO CHANGE:\n", oddVerticesList)
print("REDUCED MATRIX:\n", reducedMatrix)
finalGraph = None

if reducedMatrix.size != 0:
    linearSum = linear_sum_assignment(reducedMatrix)  # Use the linear sum to get the links to create
    print("Linear Sum:\n", linearSum)
    finalGraph = createLink(linearSum, oddVerticesList, randMat_dist, distMatrix)  # Create the links to get final graph
    print("FINAL GRAPH:\n", finalGraph)

if finalGraph is None:
    finalGraph = randMat_dist

pythonGraph = toPythonGraph(finalGraph, n)
newAdj = newAdjMatrix(pythonGraph, n)  # Create a new adjMatrix with a format that we can use in multi-graph

print("NEW ADJ MATRIX:\n", newAdj)
path = check_euler(pythonGraph, n, newAdj)  # Create the path to follow
print("PATH:\n", path)
print("DISTANCE:\n", numberKm(finalGraph, path, distMatrix), "km")  # Check the Number of km
